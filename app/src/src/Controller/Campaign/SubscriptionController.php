<?php

namespace App\Controller\Campaign;

use App\Entity\Subscription;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\IsTrue;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class SubscriptionController extends AbstractController
{
    public function index(
        Request $request,
        EntityManagerInterface $entityManager,
        \Swift_Mailer $mailer,
        LoggerInterface $logger
    ): Response {
        $form = $this->createSubscriptionForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $token = $this->generateUuid();

            $logger->info('Verification token created: ' . $token);
            /** @var Subscription $subscription */
            $subscription = $form->getData();
            $existingSubscription = $entityManager->getRepository(Subscription::class)
                ->findOneBy(['emailAddress' => $subscription->getEmailAddress()]);
            if ($existingSubscription instanceof Subscription) {
                $logger->warning('Second try with same email address');

                return $this->render(
                    'subscription.html.twig',
                    [
                        'form' => $form->createView(),
                        'error' => 'Mail-Address still exists: ' . $subscription->getEmailAddress(),
                    ]
                );
            }
            $subscription->setIsVerified(false);
            $subscription->setVerificationToken($token);
            $subscription->setCreatedDate(new \DateTime('now'));
            $entityManager->persist($subscription);
            $entityManager->flush();

            try {
                $result = $this->sendVerificationMail($mailer, $subscription, $token);
            } catch (\Swift_TransportException $e) {
                $logger->error(
                    'Problems to send mail',
                    [
                        'message' => $e->getMessage(),
                        'subscription_email' => $subscription->getEmailAddress(),
                        'subscription' => $subscription->toArray(),
                    ]
                );

                return $this->render(
                    'subscription.html.twig',
                    [
                        'form' => $form->createView(),
                        'error' => 'Problems to send Mail. Please try again.',
                    ]
                );
            }
            if ($result) {
                $logger->info('Successful subscription - Mail send');
            } else {
                $logger->error('Problems to send mail');

                return $this->render(
                    'subscription.html.twig',
                    [
                        'form' => $form->createView(),
                        'error' => 'Problems to send Mail. Please try again.',
                    ]
                );
            }

            return $this->redirectToRoute('successful_campaign_subscription');
        }

        return $this->render('subscription.html.twig', ['form' => $form->createView()]);
    }

    public function successfulSubscription(): Response
    {
        return $this->render('successful_subscription.html.twig', []);
    }

    private function createSubscriptionForm()
    {
        $subscription = new Subscription();
        $form = $this->createFormBuilder($subscription)
            ->add('titleBeforeName', TextType::class, ['label' => 'Title(s) before your name (e.g. Prof. Dr., optional)', 'required' => false])
            ->add('titleAfterName', TextType::class, ['label' => 'Title(s) after your name (e.g. ", PhD")', 'required' => false])
            ->add('highestAcademicGraduation', TextType::class, ['label' => 'Highest academic degree (e.g., bach./mast./PhD/Dr; REQUIRED)', 'required' => true])
            ->add(
                'coAuthoredPublicationLink',
                TextType::class,
                ['label' => 'IF NO Dr./PhD: Link to (co-)authored publication', 'required' => false]
            )
            ->add(
                'staffPageLink',
                TextType::class,
                ['label' => 'IF NO publicat.: Link to staff page with your name', 'required' => false]
            )
            ->add(
                'nameEmailOfSchoolarNowingYou',
                TextType::class,
                ['label' => 'IF NO staff page:Name/Email of scholar knowing you', 'required' => false]
            )
            ->add('firstName', TextType::class, ['label' => 'First Name (required, published) ', 'required' => true])
            ->add('lastName', TextType::class, ['label' => 'Last Name (required, published) ', 'required' => true])
            ->add('emailAddress', EmailType::class, ['label' => 'Email Address (required, NOT published) ', 'required' => true])
            ->add(
                'graduationState',
                ChoiceType::class,
                [
                    'label' => 'Your status (required, not published)',
                    'required' => true,
                    'choices' => [
                        'Please select ...' => '',
                        'Actively Working Scientist or Scholar' => 'Actively Working Scientist or Scholar',
                        'Former or Retired Scientist or Scholar' => 'Former or Retired Scientist or Scholar',
                        'Ph.D. student' => 'Ph.D. student',
                        'Master or similar student (only counts with verified publication)' => 'Master or similar student (only counts with verified publication)',
                        'Scientific assistant (only counts with verified publication)' => 'Scientific assistant (only counts with verified publication)',
                    ],
                    'expanded' => false,
                    'multiple' => false,
                ]
            )
            ->add(
                'publicationsState',
                ChoiceType::class,
                [
                    'label' => 'Publications (required, not published) ',
                    'required' => true,
                    'choices' => [
                        'Please select ...' => '',
                        'Never published a scholarly work' => 'Never published a scholarly work',
                        'Last publication more than 5 years ago' => 'Last publication more than 5 years ago',
                        'Last publication less than 5 years ago' => 'Last publication less than 5 years ago',
                    ],
                    'expanded' => false,
                    'multiple' => false,
                ]
            )
            ->add('department', TextType::class, ['label' => 'Department (Optional)', 'required' => false])
            ->add('organizationOrInstitute', TextType::class, ['label' => 'Organisation or Institution (Recommended)', 'required' => false])
            ->add('city', TextType::class, ['label' => 'City (Required)', 'required' => true])
            ->add('state', TextType::class, ['label' => 'State or Province (if used in your country)', 'required' => false])
            ->add('country', TextType::class, ['label' => 'Country (English name, required)', 'required' => true])
            ->add(
                'isDataPolicyAccepted',
                CheckboxType::class,
                [
                    'label' => false,
                    'constraints' => [
                        new IsTrue(
                            [
                                'message' => 'I know, it\'s hard, but you must agree to our data policy.',
                            ]
                        ),
                    ]
                ]
            )
            ->add('submit', SubmitType::class, ['label' => 'Herewith I sign the letter'])
            ->getForm();

        return $form;
    }

    private function generateUuid()
    {
        return sprintf(
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0x0C2f) | 0x4000,
            mt_rand(0, 0x3fff) | 0x8000,
            mt_rand(0, 0x2Aff),
            mt_rand(0, 0xffD3),
            mt_rand(0, 0xff4B)
        );
    }

    private function sendVerificationMail(\Swift_Mailer $mailer, Subscription $subscription, string $token): int
    {
        $message = (new \Swift_Message('Verification for Scientists for Future Signature'))
            ->setFrom('scientists.campaign@developersforfuture.org')
            ->setTo($subscription->getEmailAddress())
            ->setBody(
                $this->renderView(
                    'email_verification.html.twig',
                    [
                        'subscription' => $subscription,
                        'token' => $token,
                    ]
                ),
                'text/html'
            );

        return $mailer->send($message);
    }
}
