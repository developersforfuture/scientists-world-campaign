<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 * @ORM\Entity(repositoryClass="App\Repository\SubscriptionRepository")
 */
class Subscription
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titleBeforeName;
    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titleAfterName;
    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank
     */
    private $highestAcademicGraduation;
    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $coAuthoredPublicationLink;
    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $staffPageLink;
    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nameEmailOfSchoolarNowingYou;
    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $firstName;
    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $lastName;
    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $emailAddress;
    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $graduationState;
    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $publicationsState;
    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $department;
    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $organizationOrInstitute;
    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $city;
    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $state;
    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $country;
    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $isVerified = false;
    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $verificationToken;
    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $isDataPolicyAccepted = false;
    /**
     * @var \DateTime $created
     *
     * @ORM\Column(type="datetime")
     */
    private $createdDate;

    /**
     * @var \DateTime $updated
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $verificationDate;

    public function toArray(): array
    {
        return [
            $this->id,
            $this->titleBeforeName,
            $this->titleAfterName,
            $this->highestAcademicGraduation,
            $this->coAuthoredPublicationLink,
            $this->staffPageLink,
            $this->nameEmailOfSchoolarNowingYou,
            $this->firstName,
            $this->lastName,
            $this->emailAddress,
            $this->graduationState,
            $this->publicationsState,
            $this->department,
            $this->organizationOrInstitute,
            $this->city,
            $this->state,
            $this->country,
            $this->isVerified === false ? 0 : 1,
            $this->verificationToken,
            $this->isDataPolicyAccepted === true ? 1 : 0,
            $this->createdDate instanceof \DateTime ? $this->createdDate->format('Y-m-d H:i:s') : '',
            $this->verificationDate instanceof \DateTime ? $this->verificationDate->format('Y-m-d H:i:s') : '',
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitleBeforeName()
    {
        return $this->titleBeforeName;
    }

    /**
     * @param string $titleBeforeName
     */
    public function setTitleBeforeName($titleBeforeName)
    {
        $this->titleBeforeName = $titleBeforeName;
    }

    /**
     * @return string
     */
    public function getTitleAfterName()
    {
        return $this->titleAfterName;
    }

    /**
     * @param string $titleAfterName
     */
    public function setTitleAfterName($titleAfterName)
    {
        $this->titleAfterName = $titleAfterName;
    }

    /**
     * @return string
     */
    public function getHighestAcademicGraduation()
    {
        return $this->highestAcademicGraduation;
    }

    /**
     * @param string $highestAcademicGraduation
     */
    public function setHighestAcademicGraduation($highestAcademicGraduation)
    {
        $this->highestAcademicGraduation = $highestAcademicGraduation;
    }

    /**
     * @return string
     */
    public function getCoAuthoredPublicationLink()
    {
        return $this->coAuthoredPublicationLink;
    }

    /**
     * @param string $coAuthoredPublicationLink
     */
    public function setCoAuthoredPublicationLink($coAuthoredPublicationLink)
    {
        $this->coAuthoredPublicationLink = $coAuthoredPublicationLink;
    }

    /**
     * @return string
     */
    public function getStaffPageLink()
    {
        return $this->staffPageLink;
    }

    /**
     * @param string $staffPageLink
     */
    public function setStaffPageLink($staffPageLink)
    {
        $this->staffPageLink = $staffPageLink;
    }

    /**
     * @return string
     */
    public function getNameEmailOfSchoolarNowingYou()
    {
        return $this->nameEmailOfSchoolarNowingYou;
    }

    /**
     * @param string $nameEmailOfSchoolarNowingYou
     */
    public function setNameEmailOfSchoolarNowingYou($nameEmailOfSchoolarNowingYou)
    {
        $this->nameEmailOfSchoolarNowingYou = $nameEmailOfSchoolarNowingYou;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    /**
     * @param string $emailAddress
     */
    public function setEmailAddress($emailAddress)
    {
        $this->emailAddress = $emailAddress;
    }

    /**
     * @return string
     */
    public function getGraduationState()
    {
        return $this->graduationState;
    }

    /**
     * @param string $graduationState
     */
    public function setGraduationState($graduationState)
    {
        $this->graduationState = $graduationState;
    }

    /**
     * @return string
     */
    public function getPublicationsState()
    {
        return $this->publicationsState;
    }

    /**
     * @param string $publicationsState
     */
    public function setPublicationsState($publicationsState)
    {
        $this->publicationsState = $publicationsState;
    }

    /**
     * @return string
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param string $department
     */
    public function setDepartment($department)
    {
        $this->department = $department;
    }

    /**
     * @return string
     */
    public function getOrganizationOrInstitute()
    {
        return $this->organizationOrInstitute;
    }

    /**
     * @param string $organizationOrInstitue
     */
    public function setOrganizationOrInstitute($organizationOrInstitute)
    {
        $this->organizationOrInstitute = $organizationOrInstitute;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return bool
     */
    public function isVerified()
    {
        return $this->isVerified;
    }

    /**
     * @param bool $isVerified
     */
    public function setIsVerified($isVerified)
    {
        $this->isVerified = $isVerified;
    }

    /**
     * @return string
     */
    public function getVerificationToken()
    {
        return $this->verificationToken;
    }

    /**
     * @param string $verificationToken
     */
    public function setVerificationToken($verificationToken)
    {
        $this->verificationToken = $verificationToken;
    }

    /**
     * @return bool
     */
    public function isDataPolicyAccepted(): bool
    {
        return $this->isDataPolicyAccepted;
    }

    /**
     * @param bool $isDataPolicyAccepted
     */
    public function setIsDataPolicyAccepted(bool $isDataPolicyAccepted): void
    {
        $this->isDataPolicyAccepted = $isDataPolicyAccepted;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate(): \DateTime
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     */
    public function setCreatedDate(\DateTime $createdDate): void
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return \DateTime
     */
    public function getVerificationDate(): \DateTime
    {
        return $this->verificationDate;
    }

    /**
     * @param \DateTime $verificationDate
     */
    public function setVerificationDate(\DateTime $verificationDate): void
    {
        $this->verificationDate = $verificationDate;
    }
}
